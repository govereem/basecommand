<?php

namespace Gidsoft\Basecommand\Console;

use Symfony\Component\Console\Command\Command;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManagerFactory;
use Magento\Framework\Filesystem\Directory\ReadFactory;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManager;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

abstract class AbstractCommand extends Command
{
    protected $input;

    protected $output;

    protected $objectManager;

    protected $directoryList;

    protected $readFactory;

    protected $COMMAND = 'gidsoft:basecommand:example';

    protected $DESCRIPTION = 'Generate code: create empty module';

    protected $OPTIONS = [];

    public function __construct(
            ObjectManagerFactory $objectManagerFactory,
            DirectoryList $directoryList,
            ReadFactory $readFactory
    ) {
        parent::__construct();

        $omParams = $_SERVER;
        $omParams[StoreManager::PARAM_RUN_CODE] = 'admin';
        $omParams[Store::CUSTOM_ENTRY_POINT_PARAM] = true;

        $this->objectManager = $objectManagerFactory->create($omParams);
        $this->directoryList = $directoryList;
        $this->readFactory = $readFactory;
    }

    protected function configure()
    {
        $options = [];
        foreach($this->OPTIONS as $option) {
            $options[] = new InputArgument($option, InputArgument::OPTIONAL, str_replace('_',' ',$option));
        }

        $this->setName($this->COMMAND);
        $this->setDescription($this->DESCRIPTION);
        $this->setDefinition($options);

        parent::configure();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        if(!$this->askQuestions()) {
            $this->getOutput()->writeln('Not all questions where answered, no actions done!');
            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }

        return $this->_execute();
    }

    protected function _execute() {  }

    public function askQuestions() {

        $allAnswered = true;
        $helper = $this->getHelper('question');
        foreach($this->OPTIONS as $option) {
            if($this->getInput()->getArgument($option)) continue;

            $question = new Question('Please enter value for '.$option.':', false);
            if ($answer = $helper->ask($this->getInput(),$this->getOutput(), $question)) {
                $this->getInput()->setArgument($option,$answer);
                continue;
            }

            $allAnswered = false;
        }

        return $allAnswered;
    }

    protected function validate() {
        //TODO
    }

    public function getInput() {
        return $this->input;
    }

    public function getOutput() {
        return $this->output;
    }
}