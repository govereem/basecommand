<?php

namespace Gidsoft\Basecommand\Console\Command;

class Base extends \Gidsoft\Basecommand\Console\AbstractCommand
{

//    protected $COMMAND = 'gidsoft:basecommand:example';
//    protected $DESCRIPTION = 'Example command';
    protected $OPTIONS = [
            'vendor_name'=>'vendor_name',
            'module_name'=>'module_name'
    ];

    public function _execute()
    {
        $value = $this->getInput()->getArgument($this->OPTIONS['module_name']);
        $this->getOutput()->writeln("Show value module_name: " . $value);

        $this->getOutput()->writeln('Base command done!');

        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }


}