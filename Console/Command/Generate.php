<?php

namespace Gidsoft\Basecommand\Console\Command;

class Generate extends \Gidsoft\Basecommand\Console\AbstractCommand
{

    protected $COMMAND = 'gidsoft:basecommand:generate';
    protected $DESCRIPTION = 'Generate basecommand extension';
    protected $OPTIONS = [
            'vendor_name' => 'vendor_name',
            'module_name' => 'module_name',
            'command_name' => 'command_name'
    ];

    const COPY_FILES = [
            '/registration.php',
            '/etc/module.xml',
            '/etc/di.xml',
            '/Console/Command/Base.php'
    ];

    protected $perms;

    public function _execute()
    {
        $vendor = ucfirst(strtolower($this->getInput()->getArgument($this->OPTIONS['vendor_name'])));
        $module = ucfirst(strtolower($this->getInput()->getArgument($this->OPTIONS['module_name'])));
        $command = strtolower($this->getInput()->getArgument($this->OPTIONS['command_name']));

        $this->getOutput()->writeln('<info>Creating command: ' . $command . '</info>');

        $location = $this->generate($vendor, $module, $command);

        $this->getOutput()->writeln('<info>Base command created at! ' . $location . '</info>');
        $this->getOutput()->writeln('<comment>Please run: bin/magento module:enable ' . $vendor . '_' . $module . '</comment>');
        $this->getOutput()->writeln('<comment>Test your command, run: bin/magento ' . $command . '</comment>');

        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }


    protected function generate($vendor, $module, $command)
    {

        $this->setDirPerm();
        $path = $this->getCodePath() . $vendor;
        $copyFrom = __DIR__ . '/../../';
        $copyTo = $path . '/' . $module;

        $replaceFrom = [
                'gidsoft:basecommand:example',
                '<item name="gidsoft_generate_base_command" xsi:type="object">Gidsoft\Basecommand\Console\Command\Generate</item>',
                'gidsoft',
                'Gidsoft',
                'Basecommand',
                '\\' . $vendor . '\\' . $module . '\Console\AbstractCommand',
                '//    protected $COMMAND'
        ];

        $vendorLowercase = strtolower($vendor);
        $replaceTo = [
                strtolower($command),
                '',
                $vendorLowercase,
                $vendor,
                $module,
                '\Gidsoft\Basecommand\Console\AbstractCommand',
                '    protected $COMMAND'
        ];

        foreach (self::COPY_FILES as $filename) {
            $this->changeFileContent($filename, $copyFrom, $copyTo, $replaceFrom, $replaceTo);
        }

        return $copyTo;

    }

    protected function getCodePath()
    {
        return $this->directoryList->getPath('app') . '/code/';
    }

    protected function createDirStructure($path)
    {
        if (! file_exists($path)) {
            mkdir($path, $this->perms, true);
        }
    }

    protected function setDirPerm()
    {
        $this->perms = ($this->perms) ? $this->perms : fileperms($this->getCodePath());
    }

    protected function getDirPerm()
    {
        return $this->perms;
    }

    protected function changeFileContent($filename, $pathFrom, $pathTo, $search, $replace)
    {
        $from = $pathFrom . $filename;
        $to = $pathTo . $filename;

        $this->createDirStructure(dirname($to));
        file_put_contents($to, str_replace($search, $replace, file_get_contents($from)));
    }
}