# Magento 2 Base Command
Easy start for creating Magento 2 commands, that will ask questions when needed arguments are not entered.
After composer install and enabling this extension. You can run the command generation. 
This will setup a base console extension, with your settings.


## Install
`composer require gidsoft/basecommand dev-master`

`bin/magento module:enable Gidsoft_Basecommand`

## Test run
`bin/magento gidsoft:basecommand:example`

## How to use
Simple run: `bin/magento gidsoft:basecommand:generate vendorjoe example joe:example`


This wil generate an console extension ready to be used as you like!
The generate command will ask for vendor / module / command


To get started go to you generated extension and look for Console/Command/Base.php
and change some settings, like $OPTIONS, this will be the input arguments of your command.
```
    protected $COMMAND = 'gidsoft:basecommand:base';
    protected $DESCRIPTION = 'Base command code: simple command setup';
    protected $OPTIONS = [
            'vendor_name'=>'vendor_name',
            'module_name'=>'module_name'
    ];
```

## TODO
* Default input validation
* Easy setup optional and required arguments
* Add config class in constructor, to read magento config settings

